export default [
  {
    "label": "阿富汗 (Afghanistan)",
    "value": "阿富汗"
  },
  {
    "label": "阿爾巴尼亞 (Albania)",
    "value": "阿爾巴尼亞"
  },
  {
    "label": "阿爾及利亞 (Algeria)",
    "value": "阿爾及利亞"
  },
  {
    "label": "美屬薩摩亞 (American Samoa)",
    "value": "美屬薩摩亞"
  },
  {
    "label": "安道爾 (Andorra)",
    "value": "安道爾"
  },
  {
    "label": "安哥拉 (Angola)",
    "value": "安哥拉"
  },
  {
    "label": "安圭拉 (Anguilla)",
    "value": "安圭拉"
  },
  {
    "label": "南極洲 (Antarctica)",
    "value": "南極洲"
  },
  {
    "label": "安提瓜和巴布達 (Antigua and Barbuda)",
    "value": "安提瓜和巴布達"
  },
  {
    "label": "阿根廷 (Argentina)",
    "value": "阿根廷"
  },
  {
    "label": "亞美尼亞 (Armenia)",
    "value": "亞美尼亞"
  },
  {
    "label": "阿魯巴 (Aruba)",
    "value": "阿魯巴"
  },
  {
    "label": "澳大利亞 (Australia)",
    "value": "澳大利亞"
  },
  {
    "label": "奧地利 (Austria)",
    "value": "奧地利"
  },
  {
    "label": "阿塞拜疆 (Azerbaijan)",
    "value": "阿塞拜疆"
  },
  {
    "label": "巴林 (Bahrain)",
    "value": "巴林"
  },
  {
    "label": "孟加拉國 (Bangladesh)",
    "value": "孟加拉國"
  },
  {
    "label": "巴巴多斯 (Barbados)",
    "value": "巴巴多斯"
  },
  {
    "label": "白俄羅斯 (Belarus)",
    "value": "白俄羅斯"
  },
  {
    "label": "比利時 (Belgium)",
    "value": "比利時"
  },
  {
    "label": "伯利茲 (Belize)",
    "value": "伯利茲"
  },
  {
    "label": "貝寧 (Benin)",
    "value": "貝寧"
  },
  {
    "label": "百慕大 (Bermuda)",
    "value": "百慕大"
  },
  {
    "label": "不丹 (Bhutan)",
    "value": "不丹"
  },
  {
    "label": "玻利維亞 (Bolivia)",
    "value": "玻利維亞"
  },
  {
    "label": "波黑 (Bosnia and Herzegovina)",
    "value": "波黑"
  },
  {
    "label": "博茨瓦納 (Botswana)",
    "value": "博茨瓦納"
  },
  {
    "label": "巴西 (Brazil)",
    "value": "巴西"
  },
  {
    "label": "英屬維爾京群島 (British Virgin Islands)",
    "value": "英屬維爾京群島"
  },
  {
    "label": "文萊 (Brunei Darussalam)",
    "value": "文萊"
  },
  {
    "label": "保加利亞 (Bulgaria)",
    "value": "保加利亞"
  },
  {
    "label": "布基納法索 (Burkina Faso)",
    "value": "布基納法索"
  },
  {
    "label": "緬甸 (Burma)",
    "value": "緬甸"
  },
  {
    "label": "布隆迪 (Burundi)",
    "value": "布隆迪"
  },
  {
    "label": "柬埔寨 (Cambodia)",
    "value": "柬埔寨"
  },
  {
    "label": "喀麥隆 (Cameroon)",
    "value": "喀麥隆"
  },
  {
    "label": "加拿大 (Canada)",
    "value": "加拿大"
  },
  {
    "label": "佛得角 (Cape Verde)",
    "value": "佛得角"
  },
  {
    "label": "開曼群島 (Cayman Islands)",
    "value": "開曼群島"
  },
  {
    "label": "中非 (Central African Republic)",
    "value": "中非"
  },
  {
    "label": "乍得 (Chad)",
    "value": "乍得"
  },
  {
    "label": "智利 (Chile)",
    "value": "智利"
  },
  {
    "label": "中國 (China)",
    "value": "中國"
  },
  {
    "label": "聖誕島 (Christmas Island)",
    "value": "聖誕島"
  },
  {
    "label": "科科斯（基林）群島 (Cocos (Keeling) Islands)",
    "value": "科科斯（基林）群島"
  },
  {
    "label": "哥倫比亞 (Colombia)",
    "value": "哥倫比亞"
  },
  {
    "label": "科摩羅 (Comoros)",
    "value": "科摩羅"
  },
  {
    "label": "剛果（金） (Democratic Republic of the Congo)",
    "value": "剛果（金）"
  },
  {
    "label": "剛果（布） (Republic of the Congo)",
    "value": "剛果（布）"
  },
  {
    "label": "庫克群島 (Cook Islands)",
    "value": "庫克群島"
  },
  {
    "label": "哥斯達黎加 (Costa Rica)",
    "value": "哥斯達黎加"
  },
  {
    "label": "科特迪瓦 (Cote d'Ivoire)",
    "value": "科特迪瓦"
  },
  {
    "label": "克羅地亞 (Croatia)",
    "value": "克羅地亞"
  },
  {
    "label": "古巴 (Cuba)",
    "value": "古巴"
  },
  {
    "label": "塞浦路斯 (Cyprus)",
    "value": "塞浦路斯"
  },
  {
    "label": "捷克 (Czech Republic)",
    "value": "捷克"
  },
  {
    "label": "丹麥 (Denmark)",
    "value": "丹麥"
  },
  {
    "label": "吉布提 (Djibouti)",
    "value": "吉布提"
  },
  {
    "label": "多米尼克 (Dominica)",
    "value": "多米尼克"
  },
  {
    "label": "多米尼加 (Dominican Republic)",
    "value": "多米尼加"
  },
  {
    "label": "厄瓜多爾 (Ecuador)",
    "value": "厄瓜多爾"
  },
  {
    "label": "埃及 (Egypt)",
    "value": "埃及"
  },
  {
    "label": "薩爾瓦多 (El Salvador)",
    "value": "薩爾瓦多"
  },
  {
    "label": "赤道幾內亞 (Equatorial Guinea)",
    "value": "赤道幾內亞"
  },
  {
    "label": "厄立特里亞 (Eritrea)",
    "value": "厄立特里亞"
  },
  {
    "label": "愛沙尼亞 (Estonia)",
    "value": "愛沙尼亞"
  },
  {
    "label": "埃塞俄比亞 (Ethiopia)",
    "value": "埃塞俄比亞"
  },
  {
    "label": "福克蘭群島（馬爾維納斯） (Falkland Islands (Islas Malvinas))",
    "value": "福克蘭群島（馬爾維納斯）"
  },
  {
    "label": "法羅群島 (Faroe Islands)",
    "value": "法羅群島"
  },
  {
    "label": "斐濟 (Fiji)",
    "value": "斐濟"
  },
  {
    "label": "芬蘭 (Finland)",
    "value": "芬蘭"
  },
  {
    "label": "法國 (France)",
    "value": "法國"
  },
  {
    "label": "法屬圭亞那 (French Guiana)",
    "value": "法屬圭亞那"
  },
  {
    "label": "法屬波利尼西亞 (French Polynesia)",
    "value": "法屬波利尼西亞"
  },
  {
    "label": "加蓬 (Gabon)",
    "value": "加蓬"
  },
  {
    "label": "格魯吉亞 (Georgia)",
    "value": "格魯吉亞"
  },
  {
    "label": "德國 (Germany)",
    "value": "德國"
  },
  {
    "label": "加納 (Ghana)",
    "value": "加納"
  },
  {
    "label": "直布羅陀 (Gibraltar)",
    "value": "直布羅陀"
  },
  {
    "label": "希臘 (Greece)",
    "value": "希臘"
  },
  {
    "label": "格陵蘭 (Greenland)",
    "value": "格陵蘭"
  },
  {
    "label": "格林納達 (Grenada)",
    "value": "格林納達"
  },
  {
    "label": "瓜德羅普 (Guadeloupe)",
    "value": "瓜德羅普"
  },
  {
    "label": "關島 (Guam)",
    "value": "關島"
  },
  {
    "label": "危地馬拉 (Guatemala)",
    "value": "危地馬拉"
  },
  {
    "label": "根西島 (Guernsey)",
    "value": "根西島"
  },
  {
    "label": "幾內亞 (Guinea)",
    "value": "幾內亞"
  },
  {
    "label": "幾內亞比紹 (Guinea-Bissau)",
    "value": "幾內亞比紹"
  },
  {
    "label": "圭亞那 (Guyana)",
    "value": "圭亞那"
  },
  {
    "label": "海地 (Haiti)",
    "value": "海地"
  },
  {
    "label": "梵蒂岡 (Holy See (Vatican City))",
    "value": "梵蒂岡"
  },
  {
    "label": "洪都拉斯 (Honduras)",
    "value": "洪都拉斯"
  },
  {
    "label": "香港 (Hong Kong (SAR))",
    "value": "香港"
  },
  {
    "label": "匈牙利 (Hungary)",
    "value": "匈牙利"
  },
  {
    "label": "冰島 (Iceland)",
    "value": "冰島"
  },
  {
    "label": "印度 (India)",
    "value": "印度"
  },
  {
    "label": "印度尼西亞 (Indonesia)",
    "value": "印度尼西亞"
  },
  {
    "label": "伊朗 (Iran)",
    "value": "伊朗"
  },
  {
    "label": "伊拉克 (Iraq)",
    "value": "伊拉克"
  },
  {
    "label": " 愛爾蘭 (Ireland)",
    "value": " 愛爾蘭"
  },
  {
    "label": "以色列 (Israel)",
    "value": "以色列"
  },
  {
    "label": "意大利 (Italy)",
    "value": "意大利"
  },
  {
    "label": "牙買加 (Jamaica)",
    "value": "牙買加"
  },
  {
    "label": "日本 (Japan)",
    "value": "日本"
  },
  {
    "label": "約旦 (Jordan)",
    "value": "約旦"
  },
  {
    "label": "哈薩克斯坦 (Kazakhstan)",
    "value": "哈薩克斯坦"
  },
  {
    "label": "肯尼亞 (Kenya)",
    "value": "肯尼亞"
  },
  {
    "label": "基里巴斯 (Kiribati)",
    "value": "基里巴斯"
  },
  {
    "label": "朝鮮 (North Korea)",
    "value": "朝鮮"
  },
  {
    "label": "韓國 (South Korea)",
    "value": "韓國"
  },
  {
    "label": "科威特 (Kuwait)",
    "value": "科威特"
  },
  {
    "label": "吉爾吉斯斯坦 (Kyrgyzstan)",
    "value": "吉爾吉斯斯坦"
  },
  {
    "label": "老撾 (Laos)",
    "value": "老撾"
  },
  {
    "label": "拉脫維亞 (Latvia)",
    "value": "拉脫維亞"
  },
  {
    "label": "黎巴嫩 (Lebanon)",
    "value": "黎巴嫩"
  },
  {
    "label": "萊索托 (Lesotho)",
    "value": "萊索托"
  },
  {
    "label": "利比里亞 (Liberia)",
    "value": "利比里亞"
  },
  {
    "label": "利比亞 (Libya)",
    "value": "利比亞"
  },
  {
    "label": "列支敦士登 (Liechtenstein)",
    "value": "列支敦士登"
  },
  {
    "label": "立陶宛 (Lithuania)",
    "value": "立陶宛"
  },
  {
    "label": "盧森堡 (Luxembourg)",
    "value": "盧森堡"
  },
  {
    "label": "澳門 (Macao)",
    "value": "澳門"
  },
  {
    "label": "前南馬其頓 (The Former Yugoslav Republic of Macedonia)",
    "value": "前南馬其頓"
  },
  {
    "label": "馬達加斯加 (Madagascar)",
    "value": "馬達加斯加"
  },
  {
    "label": "馬拉維 (Malawi)",
    "value": "馬拉維"
  },
  {
    "label": "馬來西亞 (Malaysia)",
    "value": "馬來西亞"
  },
  {
    "label": "馬爾代夫 (Maldives)",
    "value": "馬爾代夫"
  },
  {
    "label": "馬里 (Mali)",
    "value": "馬里"
  },
  {
    "label": "馬耳他 (Malta)",
    "value": "馬耳他"
  },
  {
    "label": "馬紹爾群島 (Marshall Islands)",
    "value": "馬紹爾群島"
  },
  {
    "label": "馬提尼克 (Martinique)",
    "value": "馬提尼克"
  },
  {
    "label": "毛里塔尼亞 (Mauritania)",
    "value": "毛里塔尼亞"
  },
  {
    "label": "毛里求斯 (Mauritius)",
    "value": "毛里求斯"
  },
  {
    "label": "馬約特 (Mayotte)",
    "value": "馬約特"
  },
  {
    "label": "墨西哥 (Mexico)",
    "value": "墨西哥"
  },
  {
    "label": "密克羅尼西亞 (Federated States of Micronesia)",
    "value": "密克羅尼西亞"
  },
  {
    "label": "摩爾多瓦 (Moldova)",
    "value": "摩爾多瓦"
  },
  {
    "label": "摩納哥 (Monaco)",
    "value": "摩納哥"
  },
  {
    "label": "蒙古 (Mongolia)",
    "value": "蒙古"
  },
  {
    "label": "蒙特塞拉特 (Montserrat)",
    "value": "蒙特塞拉特"
  },
  {
    "label": "摩洛哥 (Morocco)",
    "value": "摩洛哥"
  },
  {
    "label": "莫桑比克 (Mozambique)",
    "value": "莫桑比克"
  },
  {
    "label": "納米尼亞 (Namibia)",
    "value": "納米尼亞"
  },
  {
    "label": "瑙魯 (Nauru)",
    "value": "瑙魯"
  },
  {
    "label": "尼泊爾 (Nepal)",
    "value": "尼泊爾"
  },
  {
    "label": "荷蘭 (Netherlands)",
    "value": "荷蘭"
  },
  {
    "label": "荷屬安的列斯 (Netherlands Antilles)",
    "value": "荷屬安的列斯"
  },
  {
    "label": "新喀裡多尼亞 (New Caledonia)",
    "value": "新喀裡多尼亞"
  },
  {
    "label": "新西蘭 (New Zealand)",
    "value": "新西蘭"
  },
  {
    "label": "尼加拉瓜 (Nicaragua)",
    "value": "尼加拉瓜"
  },
  {
    "label": "尼日爾 (Niger)",
    "value": "尼日爾"
  },
  {
    "label": "尼日利亞 (Nigeria)",
    "value": "尼日利亞"
  },
  {
    "label": "紐埃 (Niue)",
    "value": "紐埃"
  },
  {
    "label": "諾福克島 (Norfolk Island)",
    "value": "諾福克島"
  },
  {
    "label": "北馬里亞納 (Northern Mariana Islands)",
    "value": "北馬里亞納"
  },
  {
    "label": "挪威 (Norway)",
    "value": "挪威"
  },
  {
    "label": "阿曼 (Oman)",
    "value": "阿曼"
  },
  {
    "label": "巴基斯坦 (Pakistan)",
    "value": "巴基斯坦"
  },
  {
    "label": "帕勞 (Palau)",
    "value": "帕勞"
  },
  {
    "label": "巴拿馬 (Panama)",
    "value": "巴拿馬"
  },
  {
    "label": "巴布亞新幾內亞 (Papua New Guinea)",
    "value": "巴布亞新幾內亞"
  },
  {
    "label": "巴拉圭 (Paraguay)",
    "value": "巴拉圭"
  },
  {
    "label": "秘魯 (Peru)",
    "value": "秘魯"
  },
  {
    "label": "菲律賓 (Philippines)",
    "value": "菲律賓"
  },
  {
    "label": "波蘭 (Poland)",
    "value": "波蘭"
  },
  {
    "label": "葡萄牙 (Portugal)",
    "value": "葡萄牙"
  },
  {
    "label": "波多黎各 (Puerto Rico)",
    "value": "波多黎各"
  },
  {
    "label": "卡塔爾 (Qatar)",
    "value": "卡塔爾"
  },
  {
    "label": "留尼汪 (Reunion)",
    "value": "留尼汪"
  },
  {
    "label": "羅馬尼亞 (Romania)",
    "value": "羅馬尼亞"
  },
  {
    "label": "俄羅斯 (Russia)",
    "value": "俄羅斯"
  },
  {
    "label": "盧旺達 (Rwanda)",
    "value": "盧旺達"
  },
  {
    "label": "聖赫勒拿 (Saint Helena)",
    "value": "聖赫勒拿"
  },
  {
    "label": "聖基茨和尼維斯 (Saint Kitts and Nevis)",
    "value": "聖基茨和尼維斯"
  },
  {
    "label": "聖盧西亞 (Saint Lucia)",
    "value": "聖盧西亞"
  },
  {
    "label": "聖皮埃爾和密克隆 (Saint Pierre and Miquelon)",
    "value": "聖皮埃爾和密克隆"
  },
  {
    "label": "聖文森特和格林納丁斯 (Saint Vincent and the Grenadines)",
    "value": "聖文森特和格林納丁斯"
  },
  {
    "label": "薩摩亞 (Samoa)",
    "value": "薩摩亞"
  },
  {
    "label": "聖馬力諾 (San Marino)",
    "value": "聖馬力諾"
  },
  {
    "label": "聖多美和普林西比 (Sao Tome and Principe)",
    "value": "聖多美和普林西比"
  },
  {
    "label": "沙特阿拉伯 (Saudi Arabia)",
    "value": "沙特阿拉伯"
  },
  {
    "label": "塞內加爾 (Senegal)",
    "value": "塞內加爾"
  },
  {
    "label": "塞爾維亞和黑山 (Serbia and Montenegro)",
    "value": "塞爾維亞和黑山"
  },
  {
    "label": "塞舌爾 (Seychelles)",
    "value": "塞舌爾"
  },
  {
    "label": "塞拉利 (Sierra Leone)",
    "value": "塞拉利"
  },
  {
    "label": "新加坡 (Singapore)",
    "value": "新加坡"
  },
  {
    "label": "斯洛伐克 (Slovakia)",
    "value": "斯洛伐克"
  },
  {
    "label": "斯洛文尼亞 (Slovenia)",
    "value": "斯洛文尼亞"
  },
  {
    "label": "所羅門群島 (Solomon Islands)",
    "value": "所羅門群島"
  },
  {
    "label": "索馬里 (Somalia)",
    "value": "索馬里"
  },
  {
    "label": "南非 (South Africa)",
    "value": "南非"
  },
  {
    "label": "西班牙 (Spain)",
    "value": "西班牙"
  },
  {
    "label": "斯里蘭卡 (Sri Lanka)",
    "value": "斯里蘭卡"
  },
  {
    "label": "蘇丹 (Sudan)",
    "value": "蘇丹"
  },
  {
    "label": "蘇里南 (Suriname)",
    "value": "蘇里南"
  },
  {
    "label": "斯瓦爾巴島和揚馬延島 (Svalbard)",
    "value": "斯瓦爾巴島和揚馬延島"
  },
  {
    "label": "斯威士蘭 (Swaziland)",
    "value": "斯威士蘭"
  },
  {
    "label": "瑞典 (Sweden)",
    "value": "瑞典"
  },
  {
    "label": "瑞士 (Switzerland)",
    "value": "瑞士"
  },
  {
    "label": "敘利亞 (Syria)",
    "value": "敘利亞"
  },
  {
    "label": "台灣 (Taiwan)",
    "value": "台灣"
  },
  {
    "label": "塔吉克斯坦 (Tajikistan)",
    "value": "塔吉克斯坦"
  },
  {
    "label": "坦桑尼亞 (Tanzania)",
    "value": "坦桑尼亞"
  },
  {
    "label": "泰國 (Thailand)",
    "value": "泰國"
  },
  {
    "label": "巴哈馬 (The Bahamas)",
    "value": "巴哈馬"
  },
  {
    "label": "岡比亞 (The Gambia)",
    "value": "岡比亞"
  },
  {
    "label": "多哥 (Togo)",
    "value": "多哥"
  },
  {
    "label": "托克勞 (Tokelau)",
    "value": "托克勞"
  },
  {
    "label": "湯加 (Tonga)",
    "value": "湯加"
  },
  {
    "label": "特立尼達和多巴哥 (Trinidad and Tobago)",
    "value": "特立尼達和多巴哥"
  },
  {
    "label": "突尼斯 (Tunisia)",
    "value": "突尼斯"
  },
  {
    "label": "土耳其 (Turkey)",
    "value": "土耳其"
  },
  {
    "label": "土庫曼斯坦 (Turkmenistan)",
    "value": "土庫曼斯坦"
  },
  {
    "label": "特克斯和凱科斯群島 (Turks and Caicos Islands)",
    "value": "特克斯和凱科斯群島"
  },
  {
    "label": "圖瓦盧 (Tuvalu)",
    "value": "圖瓦盧"
  },
  {
    "label": "烏干達 (Uganda)",
    "value": "烏干達"
  },
  {
    "label": "烏克蘭 (Ukraine)",
    "value": "烏克蘭"
  },
  {
    "label": "阿拉伯聯合酋長國 (United Arab Emirates)",
    "value": "阿拉伯聯合酋長國"
  },
  {
    "label": "英國 (United Kingdom)",
    "value": "英國"
  },
  {
    "label": "美國 (United States)",
    "value": "美國"
  },
  {
    "label": "烏拉圭 (Uruguay)",
    "value": "烏拉圭"
  },
  {
    "label": "烏茲別克斯坦 (Uzbekistan)",
    "value": "烏茲別克斯坦"
  },
  {
    "label": "瓦努阿圖 (Vanuatu)",
    "value": "瓦努阿圖"
  },
  {
    "label": "委內瑞拉 (Venezuela)",
    "value": "委內瑞拉"
  },
  {
    "label": "越南 (Vietnam)",
    "value": "越南"
  },
  {
    "label": "美屬維爾京群島 (Virgin Islands)",
    "value": "美屬維爾京群島"
  },
  {
    "label": "瓦利斯和富圖納 (Wallis and Futuna)",
    "value": "瓦利斯和富圖納"
  },
  {
    "label": "也門 (Yemen)",
    "value": "也門"
  },
  {
    "label": "贊比亞 (Zambia)",
    "value": "贊比亞"
  },
  {
    "label": "津巴布韋 (Zimbabwe)",
    "value": "津巴布韋"
  }
]

// [
//   {
//     "label": "阿富汗",
//     "value": "阿富汗"
//   },
//   {
//     "label": "阿爾巴尼亞",
//     "value": "阿爾巴尼亞"
//   },
//   {
//     "label": "阿爾及利亞",
//     "value": "阿爾及利亞"
//   },
//   {
//     "label": "美屬薩摩亞",
//     "value": "美屬薩摩亞"
//   },
//   {
//     "label": "安道爾",
//     "value": "安道爾"
//   },
//   {
//     "label": "安哥拉",
//     "value": "安哥拉"
//   },
//   {
//     "label": "安圭拉",
//     "value": "安圭拉"
//   },
//   {
//     "label": "南極洲",
//     "value": "南極洲"
//   },
//   {
//     "label": "安提瓜和巴布達",
//     "value": "安提瓜和巴布達"
//   },
//   {
//     "label": "阿根廷",
//     "value": "阿根廷"
//   },
//   {
//     "label": "亞美尼亞",
//     "value": "亞美尼亞"
//   },
//   {
//     "label": "阿魯巴",
//     "value": "阿魯巴"
//   },
//   {
//     "label": "澳大利亞",
//     "value": "澳大利亞"
//   },
//   {
//     "label": "奧地利",
//     "value": "奧地利"
//   },
//   {
//     "label": "阿塞拜疆",
//     "value": "阿塞拜疆"
//   },
//   {
//     "label": "巴林",
//     "value": "巴林"
//   },
//   {
//     "label": "孟加拉國",
//     "value": "孟加拉國"
//   },
//   {
//     "label": "巴巴多斯",
//     "value": "巴巴多斯"
//   },
//   {
//     "label": "白俄羅斯",
//     "value": "白俄羅斯"
//   },
//   {
//     "label": "比利時",
//     "value": "比利時"
//   },
//   {
//     "label": "伯利茲",
//     "value": "伯利茲"
//   },
//   {
//     "label": "貝寧",
//     "value": "貝寧"
//   },
//   {
//     "label": "百慕大",
//     "value": "百慕大"
//   },
//   {
//     "label": "不丹",
//     "value": "不丹"
//   },
//   {
//     "label": "玻利維亞",
//     "value": "玻利維亞"
//   },
//   {
//     "label": "波黑",
//     "value": "波黑"
//   },
//   {
//     "label": "博茨瓦納",
//     "value": "博茨瓦納"
//   },
//   {
//     "label": "巴西",
//     "value": "巴西"
//   },
//   {
//     "label": "英屬維爾京群島",
//     "value": "英屬維爾京群島"
//   },
//   {
//     "label": "文萊",
//     "value": "文萊"
//   },
//   {
//     "label": "保加利亞",
//     "value": "保加利亞"
//   },
//   {
//     "label": "布基納法索",
//     "value": "布基納法索"
//   },
//   {
//     "label": "緬甸",
//     "value": "緬甸"
//   },
//   {
//     "label": "布隆迪",
//     "value": "布隆迪"
//   },
//   {
//     "label": "柬埔寨",
//     "value": "柬埔寨"
//   },
//   {
//     "label": "喀麥隆",
//     "value": "喀麥隆"
//   },
//   {
//     "label": "加拿大",
//     "value": "加拿大"
//   },
//   {
//     "label": "佛得角",
//     "value": "佛得角"
//   },
//   {
//     "label": "開曼群島",
//     "value": "開曼群島"
//   },
//   {
//     "label": "中非",
//     "value": "中非"
//   },
//   {
//     "label": "乍得",
//     "value": "乍得"
//   },
//   {
//     "label": "智利",
//     "value": "智利"
//   },
//   {
//     "label": "中國",
//     "value": "中國"
//   },
//   {
//     "label": "聖誕島",
//     "value": "聖誕島"
//   },
//   {
//     "label": "科科斯（基林）群島",
//     "value": "科科斯（基林）群島"
//   },
//   {
//     "label": "哥倫比亞",
//     "value": "哥倫比亞"
//   },
//   {
//     "label": "科摩羅",
//     "value": "科摩羅"
//   },
//   {
//     "label": "剛果（金）",
//     "value": "剛果（金）"
//   },
//   {
//     "label": "剛果（布）",
//     "value": "剛果（布）"
//   },
//   {
//     "label": "庫克群島",
//     "value": "庫克群島"
//   },
//   {
//     "label": "哥斯達黎加",
//     "value": "哥斯達黎加"
//   },
//   {
//     "label": "科特迪瓦",
//     "value": "科特迪瓦"
//   },
//   {
//     "label": "克羅地亞",
//     "value": "克羅地亞"
//   },
//   {
//     "label": "古巴",
//     "value": "古巴"
//   },
//   {
//     "label": "塞浦路斯",
//     "value": "塞浦路斯"
//   },
//   {
//     "label": "捷克",
//     "value": "捷克"
//   },
//   {
//     "label": "丹麥",
//     "value": "丹麥"
//   },
//   {
//     "label": "吉布提",
//     "value": "吉布提"
//   },
//   {
//     "label": "多米尼克",
//     "value": "多米尼克"
//   },
//   {
//     "label": "多米尼加",
//     "value": "多米尼加"
//   },
//   {
//     "label": "厄瓜多爾",
//     "value": "厄瓜多爾"
//   },
//   {
//     "label": "埃及",
//     "value": "埃及"
//   },
//   {
//     "label": "薩爾瓦多",
//     "value": "薩爾瓦多"
//   },
//   {
//     "label": "赤道幾內亞",
//     "value": "赤道幾內亞"
//   },
//   {
//     "label": "厄立特里亞",
//     "value": "厄立特里亞"
//   },
//   {
//     "label": "愛沙尼亞",
//     "value": "愛沙尼亞"
//   },
//   {
//     "label": "埃塞俄比亞",
//     "value": "埃塞俄比亞"
//   },
//   {
//     "label": "福克蘭群島（馬爾維納斯）",
//     "value": "福克蘭群島（馬爾維納斯）"
//   },
//   {
//     "label": "法羅群島",
//     "value": "法羅群島"
//   },
//   {
//     "label": "斐濟",
//     "value": "斐濟"
//   },
//   {
//     "label": "芬蘭",
//     "value": "芬蘭"
//   },
//   {
//     "label": "法國",
//     "value": "法國"
//   },
//   {
//     "label": "法屬圭亞那",
//     "value": "法屬圭亞那"
//   },
//   {
//     "label": "法屬波利尼西亞",
//     "value": "法屬波利尼西亞"
//   },
//   {
//     "label": "加蓬",
//     "value": "加蓬"
//   },
//   {
//     "label": "格魯吉亞",
//     "value": "格魯吉亞"
//   },
//   {
//     "label": "德國",
//     "value": "德國"
//   },
//   {
//     "label": "加納",
//     "value": "加納"
//   },
//   {
//     "label": "直布羅陀",
//     "value": "直布羅陀"
//   },
//   {
//     "label": "希臘",
//     "value": "希臘"
//   },
//   {
//     "label": "格陵蘭",
//     "value": "格陵蘭"
//   },
//   {
//     "label": "格林納達",
//     "value": "格林納達"
//   },
//   {
//     "label": "瓜德羅普",
//     "value": "瓜德羅普"
//   },
//   {
//     "label": "關島",
//     "value": "關島"
//   },
//   {
//     "label": "危地馬拉",
//     "value": "危地馬拉"
//   },
//   {
//     "label": "根西島",
//     "value": "根西島"
//   },
//   {
//     "label": "幾內亞",
//     "value": "幾內亞"
//   },
//   {
//     "label": "幾內亞比紹",
//     "value": "幾內亞比紹"
//   },
//   {
//     "label": "圭亞那",
//     "value": "圭亞那"
//   },
//   {
//     "label": "海地",
//     "value": "海地"
//   },
//   {
//     "label": "梵蒂岡",
//     "value": "梵蒂岡"
//   },
//   {
//     "label": "洪都拉斯",
//     "value": "洪都拉斯"
//   },
//   {
//     "label": "香港",
//     "value": "香港"
//   },
//   {
//     "label": "匈牙利",
//     "value": "匈牙利"
//   },
//   {
//     "label": "冰島",
//     "value": "冰島"
//   },
//   {
//     "label": "印度",
//     "value": "印度"
//   },
//   {
//     "label": "印度尼西亞",
//     "value": "印度尼西亞"
//   },
//   {
//     "label": "伊朗",
//     "value": "伊朗"
//   },
//   {
//     "label": "伊拉克",
//     "value": "伊拉克"
//   },
//   {
//     "label": " 愛爾蘭",
//     "value": " 愛爾蘭"
//   },
//   {
//     "label": "以色列",
//     "value": "以色列"
//   },
//   {
//     "label": "意大利",
//     "value": "意大利"
//   },
//   {
//     "label": "牙買加",
//     "value": "牙買加"
//   },
//   {
//     "label": "日本",
//     "value": "日本"
//   },
//   {
//     "label": "約旦",
//     "value": "約旦"
//   },
//   {
//     "label": "哈薩克斯坦",
//     "value": "哈薩克斯坦"
//   },
//   {
//     "label": "肯尼亞",
//     "value": "肯尼亞"
//   },
//   {
//     "label": "基里巴斯",
//     "value": "基里巴斯"
//   },
//   {
//     "label": "朝鮮",
//     "value": "朝鮮"
//   },
//   {
//     "label": "韓國",
//     "value": "韓國"
//   },
//   {
//     "label": "科威特",
//     "value": "科威特"
//   },
//   {
//     "label": "吉爾吉斯斯坦",
//     "value": "吉爾吉斯斯坦"
//   },
//   {
//     "label": "老撾",
//     "value": "老撾"
//   },
//   {
//     "label": "拉脫維亞",
//     "value": "拉脫維亞"
//   },
//   {
//     "label": "黎巴嫩",
//     "value": "黎巴嫩"
//   },
//   {
//     "label": "萊索托",
//     "value": "萊索托"
//   },
//   {
//     "label": "利比里亞",
//     "value": "利比里亞"
//   },
//   {
//     "label": "利比亞",
//     "value": "利比亞"
//   },
//   {
//     "label": "列支敦士登",
//     "value": "列支敦士登"
//   },
//   {
//     "label": "立陶宛",
//     "value": "立陶宛"
//   },
//   {
//     "label": "盧森堡",
//     "value": "盧森堡"
//   },
//   {
//     "label": "澳門",
//     "value": "澳門"
//   },
//   {
//     "label": "前南馬其頓",
//     "value": "前南馬其頓"
//   },
//   {
//     "label": "馬達加斯加",
//     "value": "馬達加斯加"
//   },
//   {
//     "label": "馬拉維",
//     "value": "馬拉維"
//   },
//   {
//     "label": "馬來西亞",
//     "value": "馬來西亞"
//   },
//   {
//     "label": "馬爾代夫",
//     "value": "馬爾代夫"
//   },
//   {
//     "label": "馬里",
//     "value": "馬里"
//   },
//   {
//     "label": "馬耳他",
//     "value": "馬耳他"
//   },
//   {
//     "label": "馬紹爾群島",
//     "value": "馬紹爾群島"
//   },
//   {
//     "label": "馬提尼克",
//     "value": "馬提尼克"
//   },
//   {
//     "label": "毛里塔尼亞",
//     "value": "毛里塔尼亞"
//   },
//   {
//     "label": "毛里求斯",
//     "value": "毛里求斯"
//   },
//   {
//     "label": "馬約特",
//     "value": "馬約特"
//   },
//   {
//     "label": "墨西哥",
//     "value": "墨西哥"
//   },
//   {
//     "label": "密克羅尼西亞",
//     "value": "密克羅尼西亞"
//   },
//   {
//     "label": "摩爾多瓦",
//     "value": "摩爾多瓦"
//   },
//   {
//     "label": "摩納哥",
//     "value": "摩納哥"
//   },
//   {
//     "label": "蒙古",
//     "value": "蒙古"
//   },
//   {
//     "label": "蒙特塞拉特",
//     "value": "蒙特塞拉特"
//   },
//   {
//     "label": "摩洛哥",
//     "value": "摩洛哥"
//   },
//   {
//     "label": "莫桑比克",
//     "value": "莫桑比克"
//   },
//   {
//     "label": "納米尼亞",
//     "value": "納米尼亞"
//   },
//   {
//     "label": "瑙魯",
//     "value": "瑙魯"
//   },
//   {
//     "label": "尼泊爾",
//     "value": "尼泊爾"
//   },
//   {
//     "label": "荷蘭",
//     "value": "荷蘭"
//   },
//   {
//     "label": "荷屬安的列斯",
//     "value": "荷屬安的列斯"
//   },
//   {
//     "label": "新喀裡多尼亞",
//     "value": "新喀裡多尼亞"
//   },
//   {
//     "label": "新西蘭",
//     "value": "新西蘭"
//   },
//   {
//     "label": "尼加拉瓜",
//     "value": "尼加拉瓜"
//   },
//   {
//     "label": "尼日爾",
//     "value": "尼日爾"
//   },
//   {
//     "label": "尼日利亞",
//     "value": "尼日利亞"
//   },
//   {
//     "label": "紐埃",
//     "value": "紐埃"
//   },
//   {
//     "label": "諾福克島",
//     "value": "諾福克島"
//   },
//   {
//     "label": "北馬里亞納",
//     "value": "北馬里亞納"
//   },
//   {
//     "label": "挪威",
//     "value": "挪威"
//   },
//   {
//     "label": "阿曼",
//     "value": "阿曼"
//   },
//   {
//     "label": "巴基斯坦",
//     "value": "巴基斯坦"
//   },
//   {
//     "label": "帕勞",
//     "value": "帕勞"
//   },
//   {
//     "label": "巴拿馬",
//     "value": "巴拿馬"
//   },
//   {
//     "label": "巴布亞新幾內亞",
//     "value": "巴布亞新幾內亞"
//   },
//   {
//     "label": "巴拉圭",
//     "value": "巴拉圭"
//   },
//   {
//     "label": "秘魯",
//     "value": "秘魯"
//   },
//   {
//     "label": "菲律賓",
//     "value": "菲律賓"
//   },
//   {
//     "label": "波蘭",
//     "value": "波蘭"
//   },
//   {
//     "label": "葡萄牙",
//     "value": "葡萄牙"
//   },
//   {
//     "label": "波多黎各",
//     "value": "波多黎各"
//   },
//   {
//     "label": "卡塔爾",
//     "value": "卡塔爾"
//   },
//   {
//     "label": "留尼汪",
//     "value": "留尼汪"
//   },
//   {
//     "label": "羅馬尼亞",
//     "value": "羅馬尼亞"
//   },
//   {
//     "label": "俄羅斯",
//     "value": "俄羅斯"
//   },
//   {
//     "label": "盧旺達",
//     "value": "盧旺達"
//   },
//   {
//     "label": "聖赫勒拿",
//     "value": "聖赫勒拿"
//   },
//   {
//     "label": "聖基茨和尼維斯",
//     "value": "聖基茨和尼維斯"
//   },
//   {
//     "label": "聖盧西亞",
//     "value": "聖盧西亞"
//   },
//   {
//     "label": "聖皮埃爾和密克隆",
//     "value": "聖皮埃爾和密克隆"
//   },
//   {
//     "label": "聖文森特和格林納丁斯",
//     "value": "聖文森特和格林納丁斯"
//   },
//   {
//     "label": "薩摩亞",
//     "value": "薩摩亞"
//   },
//   {
//     "label": "聖馬力諾",
//     "value": "聖馬力諾"
//   },
//   {
//     "label": "聖多美和普林西比",
//     "value": "聖多美和普林西比"
//   },
//   {
//     "label": "沙特阿拉伯",
//     "value": "沙特阿拉伯"
//   },
//   {
//     "label": "塞內加爾",
//     "value": "塞內加爾"
//   },
//   {
//     "label": "塞爾維亞和黑山",
//     "value": "塞爾維亞和黑山"
//   },
//   {
//     "label": "塞舌爾",
//     "value": "塞舌爾"
//   },
//   {
//     "label": "塞拉利",
//     "value": "塞拉利"
//   },
//   {
//     "label": "新加坡",
//     "value": "新加坡"
//   },
//   {
//     "label": "斯洛伐克",
//     "value": "斯洛伐克"
//   },
//   {
//     "label": "斯洛文尼亞",
//     "value": "斯洛文尼亞"
//   },
//   {
//     "label": "所羅門群島",
//     "value": "所羅門群島"
//   },
//   {
//     "label": "索馬里",
//     "value": "索馬里"
//   },
//   {
//     "label": "南非",
//     "value": "南非"
//   },
//   {
//     "label": "西班牙",
//     "value": "西班牙"
//   },
//   {
//     "label": "斯里蘭卡",
//     "value": "斯里蘭卡"
//   },
//   {
//     "label": "蘇丹",
//     "value": "蘇丹"
//   },
//   {
//     "label": "蘇里南",
//     "value": "蘇里南"
//   },
//   {
//     "label": "斯瓦爾巴島和揚馬延島",
//     "value": "斯瓦爾巴島和揚馬延島"
//   },
//   {
//     "label": "斯威士蘭",
//     "value": "斯威士蘭"
//   },
//   {
//     "label": "瑞典",
//     "value": "瑞典"
//   },
//   {
//     "label": "瑞士",
//     "value": "瑞士"
//   },
//   {
//     "label": "敘利亞",
//     "value": "敘利亞"
//   },
//   {
//     "label": "台灣",
//     "value": "台灣"
//   },
//   {
//     "label": "塔吉克斯坦",
//     "value": "塔吉克斯坦"
//   },
//   {
//     "label": "坦桑尼亞",
//     "value": "坦桑尼亞"
//   },
//   {
//     "label": "泰國",
//     "value": "泰國"
//   },
//   {
//     "label": "巴哈馬",
//     "value": "巴哈馬"
//   },
//   {
//     "label": "岡比亞",
//     "value": "岡比亞"
//   },
//   {
//     "label": "多哥",
//     "value": "多哥"
//   },
//   {
//     "label": "托克勞",
//     "value": "托克勞"
//   },
//   {
//     "label": "湯加",
//     "value": "湯加"
//   },
//   {
//     "label": "特立尼達和多巴哥",
//     "value": "特立尼達和多巴哥"
//   },
//   {
//     "label": "突尼斯",
//     "value": "突尼斯"
//   },
//   {
//     "label": "土耳其",
//     "value": "土耳其"
//   },
//   {
//     "label": "土庫曼斯坦",
//     "value": "土庫曼斯坦"
//   },
//   {
//     "label": "特克斯和凱科斯群島",
//     "value": "特克斯和凱科斯群島"
//   },
//   {
//     "label": "圖瓦盧",
//     "value": "圖瓦盧"
//   },
//   {
//     "label": "烏干達",
//     "value": "烏干達"
//   },
//   {
//     "label": "烏克蘭",
//     "value": "烏克蘭"
//   },
//   {
//     "label": "阿拉伯聯合酋長國",
//     "value": "阿拉伯聯合酋長國"
//   },
//   {
//     "label": "英國",
//     "value": "英國"
//   },
//   {
//     "label": "美國",
//     "value": "美國"
//   },
//   {
//     "label": "烏拉圭",
//     "value": "烏拉圭"
//   },
//   {
//     "label": "烏茲別克斯坦",
//     "value": "烏茲別克斯坦"
//   },
//   {
//     "label": "瓦努阿圖",
//     "value": "瓦努阿圖"
//   },
//   {
//     "label": "委內瑞拉",
//     "value": "委內瑞拉"
//   },
//   {
//     "label": "越南",
//     "value": "越南"
//   },
//   {
//     "label": "美屬維爾京群島",
//     "value": "美屬維爾京群島"
//   },
//   {
//     "label": "瓦利斯和富圖納",
//     "value": "瓦利斯和富圖納"
//   },
//   {
//     "label": "也門",
//     "value": "也門"
//   },
//   {
//     "label": "贊比亞",
//     "value": "贊比亞"
//   },
//   {
//     "label": "津巴布韋",
//     "value": "津巴布韋"
//   }
// ]


// export default [
//   {
//     "index": 0,
//     "zhTw": "阿富汗",
//     "en": "Afghanistan",
//     "phoneCode": "+93"
//   },
//   {
//     "index": 1,
//     "zhTw": "阿爾巴尼亞",
//     "en": "Albania",
//     "phoneCode": "+355"
//   },
//   {
//     "index": 2,
//     "zhTw": "阿爾及利亞",
//     "en": "Algeria",
//     "phoneCode": "+213"
//   },
//   {
//     "index": 3,
//     "zhTw": "美屬薩摩亞",
//     "en": "American Samoa",
//     "phoneCode": "+684"
//   },
//   {
//     "index": 4,
//     "zhTw": "安道爾",
//     "en": "Andorra",
//     "phoneCode": "+376"
//   },
//   {
//     "index": 5,
//     "zhTw": "安哥拉",
//     "en": "Angola",
//     "phoneCode": "+244"
//   },
//   {
//     "index": 6,
//     "zhTw": "安圭拉",
//     "en": "Anguilla",
//     "phoneCode": "+1264"
//   },
//   {
//     "index": 7,
//     "zhTw": "南極洲",
//     "en": "Antarctica",
//     "phoneCode": "+672"
//   },
//   {
//     "index": 8,
//     "zhTw": "安提瓜和巴布達",
//     "en": "Antigua and Barbuda",
//     "phoneCode": "+1268"
//   },
//   {
//     "index": 9,
//     "zhTw": "阿根廷",
//     "en": "Argentina",
//     "phoneCode": "+54"
//   },
//   {
//     "index": 10,
//     "zhTw": "亞美尼亞",
//     "en": "Armenia",
//     "phoneCode": "+374"
//   },
//   {
//     "index": 11,
//     "zhTw": "阿魯巴",
//     "en": "Aruba",
//     "phoneCode": "+297"
//   },
//   {
//     "index": 12,
//     "zhTw": "澳大利亞",
//     "en": "Australia",
//     "phoneCode": "+61"
//   },
//   {
//     "index": 13,
//     "zhTw": "奧地利",
//     "en": "Austria",
//     "phoneCode": "+43"
//   },
//   {
//     "index": 14,
//     "zhTw": "阿塞拜疆",
//     "en": "Azerbaijan",
//     "phoneCode": "+994"
//   },
//   {
//     "index": 15,
//     "zhTw": "巴林",
//     "en": "Bahrain",
//     "phoneCode": "+973"
//   },
//   {
//     "index": 16,
//     "zhTw": "孟加拉國",
//     "en": "Bangladesh",
//     "phoneCode": "+880"
//   },
//   {
//     "index": 17,
//     "zhTw": "巴巴多斯",
//     "en": "Barbados",
//     "phoneCode": "+1246"
//   },
//   {
//     "index": 18,
//     "zhTw": "白俄羅斯",
//     "en": "Belarus",
//     "phoneCode": "+375"
//   },
//   {
//     "index": 19,
//     "zhTw": "比利時",
//     "en": "Belgium",
//     "phoneCode": "+32"
//   },
//   {
//     "index": 20,
//     "zhTw": "伯利茲",
//     "en": "Belize",
//     "phoneCode": "+501"
//   },
//   {
//     "index": 21,
//     "zhTw": "貝寧",
//     "en": "Benin",
//     "phoneCode": "+229"
//   },
//   {
//     "index": 22,
//     "zhTw": "百慕大",
//     "en": "Bermuda",
//     "phoneCode": "+1441"
//   },
//   {
//     "index": 23,
//     "zhTw": "不丹",
//     "en": "Bhutan",
//     "phoneCode": "+975"
//   },
//   {
//     "index": 24,
//     "zhTw": "玻利維亞",
//     "en": "Bolivia",
//     "phoneCode": "+591"
//   },
//   {
//     "index": 25,
//     "zhTw": "波黑",
//     "en": "Bosnia and Herzegovina",
//     "phoneCode": "+387"
//   },
//   {
//     "index": 26,
//     "zhTw": "博茨瓦納",
//     "en": "Botswana",
//     "phoneCode": "+267"
//   },
//   {
//     "index": 27,
//     "zhTw": "巴西",
//     "en": "Brazil",
//     "phoneCode": "+55"
//   },
//   {
//     "index": 28,
//     "zhTw": "英屬維爾京群島",
//     "en": "British Virgin Islands",
//     "phoneCode": "+1284"
//   },
//   {
//     "index": 29,
//     "zhTw": "文萊",
//     "en": "Brunei Darussalam",
//     "phoneCode": "+673"
//   },
//   {
//     "index": 30,
//     "zhTw": "保加利亞",
//     "en": "Bulgaria",
//     "phoneCode": "+359"
//   },
//   {
//     "index": 31,
//     "zhTw": "布基納法索",
//     "en": "Burkina Faso",
//     "phoneCode": "+226"
//   },
//   {
//     "index": 32,
//     "zhTw": "緬甸",
//     "en": "Burma",
//     "phoneCode": "+95"
//   },
//   {
//     "index": 33,
//     "zhTw": "布隆迪",
//     "en": "Burundi",
//     "phoneCode": "+257"
//   },
//   {
//     "index": 34,
//     "zhTw": "柬埔寨",
//     "en": "Cambodia",
//     "phoneCode": "+855"
//   },
//   {
//     "index": 35,
//     "zhTw": "喀麥隆",
//     "en": "Cameroon",
//     "phoneCode": "+237"
//   },
//   {
//     "index": 36,
//     "zhTw": "加拿大",
//     "en": "Canada",
//     "phoneCode": "+1"
//   },
//   {
//     "index": 37,
//     "zhTw": "佛得角",
//     "en": "Cape Verde",
//     "phoneCode": "+238"
//   },
//   {
//     "index": 38,
//     "zhTw": "開曼群島",
//     "en": "Cayman Islands",
//     "phoneCode": "+1345"
//   },
//   {
//     "index": 39,
//     "zhTw": "中非",
//     "en": "Central African Republic",
//     "phoneCode": "+236"
//   },
//   {
//     "index": 40,
//     "zhTw": "乍得",
//     "en": "Chad",
//     "phoneCode": "+235"
//   },
//   {
//     "index": 41,
//     "zhTw": "智利",
//     "en": "Chile",
//     "phoneCode": "+56"
//   },
//   {
//     "index": 42,
//     "zhTw": "中國",
//     "en": "China",
//     "phoneCode": "+86"
//   },
//   {
//     "index": 43,
//     "zhTw": "聖誕島",
//     "en": "Christmas Island",
//     "phoneCode": "+61"
//   },
//   {
//     "index": 44,
//     "zhTw": "科科斯（基林）群島",
//     "en": "Cocos (Keeling) Islands",
//     "phoneCode": "+61"
//   },
//   {
//     "index": 45,
//     "zhTw": "哥倫比亞",
//     "en": "Colombia",
//     "phoneCode": "+57"
//   },
//   {
//     "index": 46,
//     "zhTw": "科摩羅",
//     "en": "Comoros",
//     "phoneCode": "+269"
//   },
//   {
//     "index": 47,
//     "zhTw": "剛果（金）",
//     "en": "Democratic Republic of the Congo",
//     "phoneCode": "+243"
//   },
//   {
//     "index": 48,
//     "zhTw": "剛果（布）",
//     "en": "Republic of the Congo",
//     "phoneCode": "+242"
//   },
//   {
//     "index": 49,
//     "zhTw": "庫克群島",
//     "en": "Cook Islands",
//     "phoneCode": "+682"
//   },
//   {
//     "index": 50,
//     "zhTw": "哥斯達黎加",
//     "en": "Costa Rica",
//     "phoneCode": "+506"
//   },
//   {
//     "index": 51,
//     "zhTw": "科特迪瓦",
//     "en": "Cote d'Ivoire",
//     "phoneCode": "+225"
//   },
//   {
//     "index": 52,
//     "zhTw": "克羅地亞",
//     "en": "Croatia",
//     "phoneCode": "+385"
//   },
//   {
//     "index": 53,
//     "zhTw": "古巴",
//     "en": "Cuba",
//     "phoneCode": "+53"
//   },
//   {
//     "index": 54,
//     "zhTw": "塞浦路斯",
//     "en": "Cyprus",
//     "phoneCode": "+357"
//   },
//   {
//     "index": 55,
//     "zhTw": "捷克",
//     "en": "Czech Republic",
//     "phoneCode": "+420"
//   },
//   {
//     "index": 56,
//     "zhTw": "丹麥",
//     "en": "Denmark",
//     "phoneCode": "+45"
//   },
//   {
//     "index": 57,
//     "zhTw": "吉布提",
//     "en": "Djibouti",
//     "phoneCode": "+253"
//   },
//   {
//     "index": 58,
//     "zhTw": "多米尼克",
//     "en": "Dominica",
//     "phoneCode": "+1767"
//   },
//   {
//     "index": 59,
//     "zhTw": "多米尼加",
//     "en": "Dominican Republic",
//     "phoneCode": "+1809"
//   },
//   {
//     "index": 60,
//     "zhTw": "厄瓜多爾",
//     "en": "Ecuador",
//     "phoneCode": "+593"
//   },
//   {
//     "index": 61,
//     "zhTw": "埃及",
//     "en": "Egypt",
//     "phoneCode": "+20"
//   },
//   {
//     "index": 62,
//     "zhTw": "薩爾瓦多",
//     "en": "El Salvador",
//     "phoneCode": "+503"
//   },
//   {
//     "index": 63,
//     "zhTw": "赤道幾內亞",
//     "en": "Equatorial Guinea",
//     "phoneCode": "+240"
//   },
//   {
//     "index": 64,
//     "zhTw": "厄立特里亞",
//     "en": "Eritrea",
//     "phoneCode": "+291"
//   },
//   {
//     "index": 65,
//     "zhTw": "愛沙尼亞",
//     "en": "Estonia",
//     "phoneCode": "+372"
//   },
//   {
//     "index": 66,
//     "zhTw": "埃塞俄比亞",
//     "en": "Ethiopia",
//     "phoneCode": "+251"
//   },
//   {
//     "index": 67,
//     "zhTw": "福克蘭群島（馬爾維納斯）",
//     "en": "Falkland Islands (Islas Malvinas)",
//     "phoneCode": "+500"
//   },
//   {
//     "index": 68,
//     "zhTw": "法羅群島",
//     "en": "Faroe Islands",
//     "phoneCode": "+298"
//   },
//   {
//     "index": 69,
//     "zhTw": "斐濟",
//     "en": "Fiji",
//     "phoneCode": "+679"
//   },
//   {
//     "index": 70,
//     "zhTw": "芬蘭",
//     "en": "Finland",
//     "phoneCode": "+358"
//   },
//   {
//     "index": 71,
//     "zhTw": "法國",
//     "en": "France",
//     "phoneCode": "+33"
//   },
//   {
//     "index": 72,
//     "zhTw": "法屬圭亞那",
//     "en": "French Guiana",
//     "phoneCode": "+594"
//   },
//   {
//     "index": 73,
//     "zhTw": "法屬波利尼西亞",
//     "en": "French Polynesia",
//     "phoneCode": "+689"
//   },
//   {
//     "index": 74,
//     "zhTw": "加蓬",
//     "en": "Gabon",
//     "phoneCode": "+241"
//   },
//   {
//     "index": 75,
//     "zhTw": "格魯吉亞",
//     "en": "Georgia",
//     "phoneCode": "+995"
//   },
//   {
//     "index": 76,
//     "zhTw": "德國",
//     "en": "Germany",
//     "phoneCode": "+49"
//   },
//   {
//     "index": 77,
//     "zhTw": "加納",
//     "en": "Ghana",
//     "phoneCode": "+233"
//   },
//   {
//     "index": 78,
//     "zhTw": "直布羅陀",
//     "en": "Gibraltar",
//     "phoneCode": "+350"
//   },
//   {
//     "index": 79,
//     "zhTw": "希臘",
//     "en": "Greece",
//     "phoneCode": "+30"
//   },
//   {
//     "index": 80,
//     "zhTw": "格陵蘭",
//     "en": "Greenland",
//     "phoneCode": "+299"
//   },
//   {
//     "index": 81,
//     "zhTw": "格林納達",
//     "en": "Grenada",
//     "phoneCode": "+1473"
//   },
//   {
//     "index": 82,
//     "zhTw": "瓜德羅普",
//     "en": "Guadeloupe",
//     "phoneCode": "+590"
//   },
//   {
//     "index": 83,
//     "zhTw": "關島",
//     "en": "Guam",
//     "phoneCode": "+1671"
//   },
//   {
//     "index": 84,
//     "zhTw": "危地馬拉",
//     "en": "Guatemala",
//     "phoneCode": "+502"
//   },
//   {
//     "index": 85,
//     "zhTw": "根西島",
//     "en": "Guernsey",
//     "phoneCode": "+1481"
//   },
//   {
//     "index": 86,
//     "zhTw": "幾內亞",
//     "en": "Guinea",
//     "phoneCode": "+224"
//   },
//   {
//     "index": 87,
//     "zhTw": "幾內亞比紹",
//     "en": "Guinea-Bissau",
//     "phoneCode": "+245"
//   },
//   {
//     "index": 88,
//     "zhTw": "圭亞那",
//     "en": "Guyana",
//     "phoneCode": "+592"
//   },
//   {
//     "index": 89,
//     "zhTw": "海地",
//     "en": "Haiti",
//     "phoneCode": "+509"
//   },
//   {
//     "index": 90,
//     "zhTw": "梵蒂岡",
//     "en": "Holy See (Vatican City)",
//     "phoneCode": "+379"
//   },
//   {
//     "index": 91,
//     "zhTw": "洪都拉斯",
//     "en": "Honduras",
//     "phoneCode": "+504"
//   },
//   {
//     "index": 92,
//     "zhTw": "香港",
//     "en": "Hong Kong (SAR)",
//     "phoneCode": "+852"
//   },
//   {
//     "index": 93,
//     "zhTw": "匈牙利",
//     "en": "Hungary",
//     "phoneCode": "+36"
//   },
//   {
//     "index": 94,
//     "zhTw": "冰島",
//     "en": "Iceland",
//     "phoneCode": "+354"
//   },
//   {
//     "index": 95,
//     "zhTw": "印度",
//     "en": "India",
//     "phoneCode": "+91"
//   },
//   {
//     "index": 96,
//     "zhTw": "印度尼西亞",
//     "en": "Indonesia",
//     "phoneCode": "+62"
//   },
//   {
//     "index": 97,
//     "zhTw": "伊朗",
//     "en": "Iran",
//     "phoneCode": "+98"
//   },
//   {
//     "index": 98,
//     "zhTw": "伊拉克",
//     "en": "Iraq",
//     "phoneCode": "+964"
//   },
//   {
//     "index": 99,
//     "zhTw": " 愛爾蘭",
//     "en": "Ireland",
//     "phoneCode": "+353"
//   },
//   {
//     "index": 100,
//     "zhTw": "以色列",
//     "en": "Israel",
//     "phoneCode": "+972"
//   },
//   {
//     "index": 101,
//     "zhTw": "意大利",
//     "en": "Italy",
//     "phoneCode": "+39"
//   },
//   {
//     "index": 102,
//     "zhTw": "牙買加",
//     "en": "Jamaica",
//     "phoneCode": "+1876"
//   },
//   {
//     "index": 103,
//     "zhTw": "日本",
//     "en": "Japan",
//     "phoneCode": "+81"
//   },
//   {
//     "index": 104,
//     "zhTw": "約旦",
//     "en": "Jordan",
//     "phoneCode": "+962"
//   },
//   {
//     "index": 105,
//     "zhTw": "哈薩克斯坦",
//     "en": "Kazakhstan",
//     "phoneCode": "+73"
//   },
//   {
//     "index": 106,
//     "zhTw": "肯尼亞",
//     "en": "Kenya",
//     "phoneCode": "+254"
//   },
//   {
//     "index": 107,
//     "zhTw": "基里巴斯",
//     "en": "Kiribati",
//     "phoneCode": "+686"
//   },
//   {
//     "index": 108,
//     "zhTw": "朝鮮",
//     "en": "North Korea",
//     "phoneCode": "+850"
//   },
//   {
//     "index": 109,
//     "zhTw": "韓國",
//     "en": "South Korea",
//     "phoneCode": "+82"
//   },
//   {
//     "index": 110,
//     "zhTw": "科威特",
//     "en": "Kuwait",
//     "phoneCode": "+965"
//   },
//   {
//     "index": 111,
//     "zhTw": "吉爾吉斯斯坦",
//     "en": "Kyrgyzstan",
//     "phoneCode": "+996"
//   },
//   {
//     "index": 112,
//     "zhTw": "老撾",
//     "en": "Laos",
//     "phoneCode": "+856"
//   },
//   {
//     "index": 113,
//     "zhTw": "拉脫維亞",
//     "en": "Latvia",
//     "phoneCode": "+371"
//   },
//   {
//     "index": 114,
//     "zhTw": "黎巴嫩",
//     "en": "Lebanon",
//     "phoneCode": "+961"
//   },
//   {
//     "index": 115,
//     "zhTw": "萊索托",
//     "en": "Lesotho",
//     "phoneCode": "+266"
//   },
//   {
//     "index": 116,
//     "zhTw": "利比里亞",
//     "en": "Liberia",
//     "phoneCode": "+231"
//   },
//   {
//     "index": 117,
//     "zhTw": "利比亞",
//     "en": "Libya",
//     "phoneCode": "+218"
//   },
//   {
//     "index": 118,
//     "zhTw": "列支敦士登",
//     "en": "Liechtenstein",
//     "phoneCode": "+423"
//   },
//   {
//     "index": 119,
//     "zhTw": "立陶宛",
//     "en": "Lithuania",
//     "phoneCode": "+370"
//   },
//   {
//     "index": 120,
//     "zhTw": "盧森堡",
//     "en": "Luxembourg",
//     "phoneCode": "+352"
//   },
//   {
//     "index": 121,
//     "zhTw": "澳門",
//     "en": "Macao",
//     "phoneCode": "+853"
//   },
//   {
//     "index": 122,
//     "zhTw": "前南馬其頓",
//     "en": "The Former Yugoslav Republic of Macedonia",
//     "phoneCode": "+389"
//   },
//   {
//     "index": 123,
//     "zhTw": "馬達加斯加",
//     "en": "Madagascar",
//     "phoneCode": "+261"
//   },
//   {
//     "index": 124,
//     "zhTw": "馬拉維",
//     "en": "Malawi",
//     "phoneCode": "+265"
//   },
//   {
//     "index": 125,
//     "zhTw": "馬來西亞",
//     "en": "Malaysia",
//     "phoneCode": "+60"
//   },
//   {
//     "index": 126,
//     "zhTw": "馬爾代夫",
//     "en": "Maldives",
//     "phoneCode": "+960"
//   },
//   {
//     "index": 127,
//     "zhTw": "馬里",
//     "en": "Mali",
//     "phoneCode": "+223"
//   },
//   {
//     "index": 128,
//     "zhTw": "馬耳他",
//     "en": "Malta",
//     "phoneCode": "+356"
//   },
//   {
//     "index": 129,
//     "zhTw": "馬紹爾群島",
//     "en": "Marshall Islands",
//     "phoneCode": "+692"
//   },
//   {
//     "index": 130,
//     "zhTw": "馬提尼克",
//     "en": "Martinique",
//     "phoneCode": "+596"
//   },
//   {
//     "index": 131,
//     "zhTw": "毛里塔尼亞",
//     "en": "Mauritania",
//     "phoneCode": "+222"
//   },
//   {
//     "index": 132,
//     "zhTw": "毛里求斯",
//     "en": "Mauritius",
//     "phoneCode": "+230"
//   },
//   {
//     "index": 133,
//     "zhTw": "馬約特",
//     "en": "Mayotte",
//     "phoneCode": "+269"
//   },
//   {
//     "index": 134,
//     "zhTw": "墨西哥",
//     "en": "Mexico",
//     "phoneCode": "+52"
//   },
//   {
//     "index": 135,
//     "zhTw": "密克羅尼西亞",
//     "en": "Federated States of Micronesia",
//     "phoneCode": "+691"
//   },
//   {
//     "index": 136,
//     "zhTw": "摩爾多瓦",
//     "en": "Moldova",
//     "phoneCode": "+373"
//   },
//   {
//     "index": 137,
//     "zhTw": "摩納哥",
//     "en": "Monaco",
//     "phoneCode": "+377"
//   },
//   {
//     "index": 138,
//     "zhTw": "蒙古",
//     "en": "Mongolia",
//     "phoneCode": "+976"
//   },
//   {
//     "index": 139,
//     "zhTw": "蒙特塞拉特",
//     "en": "Montserrat",
//     "phoneCode": "+1664"
//   },
//   {
//     "index": 140,
//     "zhTw": "摩洛哥",
//     "en": "Morocco",
//     "phoneCode": "+212"
//   },
//   {
//     "index": 141,
//     "zhTw": "莫桑比克",
//     "en": "Mozambique",
//     "phoneCode": "+258"
//   },
//   {
//     "index": 142,
//     "zhTw": "納米尼亞",
//     "en": "Namibia",
//     "phoneCode": "+264"
//   },
//   {
//     "index": 143,
//     "zhTw": "瑙魯",
//     "en": "Nauru",
//     "phoneCode": "+674"
//   },
//   {
//     "index": 144,
//     "zhTw": "尼泊爾",
//     "en": "Nepal",
//     "phoneCode": "+977"
//   },
//   {
//     "index": 145,
//     "zhTw": "荷蘭",
//     "en": "Netherlands",
//     "phoneCode": "+31"
//   },
//   {
//     "index": 146,
//     "zhTw": "荷屬安的列斯",
//     "en": "Netherlands Antilles",
//     "phoneCode": "+599"
//   },
//   {
//     "index": 147,
//     "zhTw": "新喀裡多尼亞",
//     "en": "New Caledonia",
//     "phoneCode": "+687"
//   },
//   {
//     "index": 148,
//     "zhTw": "新西蘭",
//     "en": "New Zealand",
//     "phoneCode": "+64"
//   },
//   {
//     "index": 149,
//     "zhTw": "尼加拉瓜",
//     "en": "Nicaragua",
//     "phoneCode": "+505"
//   },
//   {
//     "index": 150,
//     "zhTw": "尼日爾",
//     "en": "Niger",
//     "phoneCode": "+227"
//   },
//   {
//     "index": 151,
//     "zhTw": "尼日利亞",
//     "en": "Nigeria",
//     "phoneCode": "+234"
//   },
//   {
//     "index": 152,
//     "zhTw": "紐埃",
//     "en": "Niue",
//     "phoneCode": "+683"
//   },
//   {
//     "index": 153,
//     "zhTw": "諾福克島",
//     "en": "Norfolk Island",
//     "phoneCode": "+6723"
//   },
//   {
//     "index": 154,
//     "zhTw": "北馬里亞納",
//     "en": "Northern Mariana Islands",
//     "phoneCode": "+1"
//   },
//   {
//     "index": 155,
//     "zhTw": "挪威",
//     "en": "Norway",
//     "phoneCode": "+47"
//   },
//   {
//     "index": 156,
//     "zhTw": "阿曼",
//     "en": "Oman",
//     "phoneCode": "+968"
//   },
//   {
//     "index": 157,
//     "zhTw": "巴基斯坦",
//     "en": "Pakistan",
//     "phoneCode": "+92"
//   },
//   {
//     "index": 158,
//     "zhTw": "帕勞",
//     "en": "Palau",
//     "phoneCode": "+680"
//   },
//   {
//     "index": 159,
//     "zhTw": "巴拿馬",
//     "en": "Panama",
//     "phoneCode": "+507"
//   },
//   {
//     "index": 160,
//     "zhTw": "巴布亞新幾內亞",
//     "en": "Papua New Guinea",
//     "phoneCode": "+675"
//   },
//   {
//     "index": 161,
//     "zhTw": "巴拉圭",
//     "en": "Paraguay",
//     "phoneCode": "+595"
//   },
//   {
//     "index": 162,
//     "zhTw": "秘魯",
//     "en": "Peru",
//     "phoneCode": "+51"
//   },
//   {
//     "index": 163,
//     "zhTw": "菲律賓",
//     "en": "Philippines",
//     "phoneCode": "+63"
//   },
//   {
//     "index": 164,
//     "zhTw": "波蘭",
//     "en": "Poland",
//     "phoneCode": "+48"
//   },
//   {
//     "index": 165,
//     "zhTw": "葡萄牙",
//     "en": "Portugal",
//     "phoneCode": "+351"
//   },
//   {
//     "index": 166,
//     "zhTw": "波多黎各",
//     "en": "Puerto Rico",
//     "phoneCode": "+1809"
//   },
//   {
//     "index": 167,
//     "zhTw": "卡塔爾",
//     "en": "Qatar",
//     "phoneCode": "+974"
//   },
//   {
//     "index": 168,
//     "zhTw": "留尼汪",
//     "en": "Reunion",
//     "phoneCode": "+262"
//   },
//   {
//     "index": 169,
//     "zhTw": "羅馬尼亞",
//     "en": "Romania",
//     "phoneCode": "+40"
//   },
//   {
//     "index": 170,
//     "zhTw": "俄羅斯",
//     "en": "Russia",
//     "phoneCode": "+7"
//   },
//   {
//     "index": 171,
//     "zhTw": "盧旺達",
//     "en": "Rwanda",
//     "phoneCode": "+250"
//   },
//   {
//     "index": 172,
//     "zhTw": "聖赫勒拿",
//     "en": "Saint Helena",
//     "phoneCode": "+290"
//   },
//   {
//     "index": 173,
//     "zhTw": "聖基茨和尼維斯",
//     "en": "Saint Kitts and Nevis",
//     "phoneCode": "+1869"
//   },
//   {
//     "index": 174,
//     "zhTw": "聖盧西亞",
//     "en": "Saint Lucia",
//     "phoneCode": "+1758"
//   },
//   {
//     "index": 175,
//     "zhTw": "聖皮埃爾和密克隆",
//     "en": "Saint Pierre and Miquelon",
//     "phoneCode": "+508"
//   },
//   {
//     "index": 176,
//     "zhTw": "聖文森特和格林納丁斯",
//     "en": "Saint Vincent and the Grenadines",
//     "phoneCode": "+1784"
//   },
//   {
//     "index": 177,
//     "zhTw": "薩摩亞",
//     "en": "Samoa",
//     "phoneCode": "+685"
//   },
//   {
//     "index": 178,
//     "zhTw": "聖馬力諾",
//     "en": "San Marino",
//     "phoneCode": "+378"
//   },
//   {
//     "index": 179,
//     "zhTw": "聖多美和普林西比",
//     "en": "Sao Tome and Principe",
//     "phoneCode": "+239"
//   },
//   {
//     "index": 180,
//     "zhTw": "沙特阿拉伯",
//     "en": "Saudi Arabia",
//     "phoneCode": "+966"
//   },
//   {
//     "index": 181,
//     "zhTw": "塞內加爾",
//     "en": "Senegal",
//     "phoneCode": "+221"
//   },
//   {
//     "index": 182,
//     "zhTw": "塞爾維亞和黑山",
//     "en": "Serbia and Montenegro",
//     "phoneCode": "+381"
//   },
//   {
//     "index": 183,
//     "zhTw": "塞舌爾",
//     "en": "Seychelles",
//     "phoneCode": "+248"
//   },
//   {
//     "index": 184,
//     "zhTw": "塞拉利",
//     "en": "Sierra Leone",
//     "phoneCode": "+232"
//   },
//   {
//     "index": 185,
//     "zhTw": "新加坡",
//     "en": "Singapore",
//     "phoneCode": "+65"
//   },
//   {
//     "index": 186,
//     "zhTw": "斯洛伐克",
//     "en": "Slovakia",
//     "phoneCode": "+421"
//   },
//   {
//     "index": 187,
//     "zhTw": "斯洛文尼亞",
//     "en": "Slovenia",
//     "phoneCode": "+386"
//   },
//   {
//     "index": 188,
//     "zhTw": "所羅門群島",
//     "en": "Solomon Islands",
//     "phoneCode": "+677"
//   },
//   {
//     "index": 189,
//     "zhTw": "索馬里",
//     "en": "Somalia",
//     "phoneCode": "+252"
//   },
//   {
//     "index": 190,
//     "zhTw": "南非",
//     "en": "South Africa",
//     "phoneCode": "+27"
//   },
//   {
//     "index": 191,
//     "zhTw": "西班牙",
//     "en": "Spain",
//     "phoneCode": "+34"
//   },
//   {
//     "index": 192,
//     "zhTw": "斯里蘭卡",
//     "en": "Sri Lanka",
//     "phoneCode": "+94"
//   },
//   {
//     "index": 193,
//     "zhTw": "蘇丹",
//     "en": "Sudan",
//     "phoneCode": "+249"
//   },
//   {
//     "index": 194,
//     "zhTw": "蘇里南",
//     "en": "Suriname",
//     "phoneCode": "+597"
//   },
//   {
//     "index": 195,
//     "zhTw": "斯瓦爾巴島和揚馬延島",
//     "en": "Svalbard",
//     "phoneCode": "+47"
//   },
//   {
//     "index": 196,
//     "zhTw": "斯威士蘭",
//     "en": "Swaziland",
//     "phoneCode": "+268"
//   },
//   {
//     "index": 197,
//     "zhTw": "瑞典",
//     "en": "Sweden",
//     "phoneCode": "+46"
//   },
//   {
//     "index": 198,
//     "zhTw": "瑞士",
//     "en": "Switzerland",
//     "phoneCode": "+41"
//   },
//   {
//     "index": 199,
//     "zhTw": "敘利亞",
//     "en": "Syria",
//     "phoneCode": "+963"
//   },
//   {
//     "index": 200,
//     "zhTw": "台灣",
//     "en": "Taiwan",
//     "phoneCode": "+886"
//   },
//   {
//     "index": 201,
//     "zhTw": "塔吉克斯坦",
//     "en": "Tajikistan",
//     "phoneCode": "+992"
//   },
//   {
//     "index": 202,
//     "zhTw": "坦桑尼亞",
//     "en": "Tanzania",
//     "phoneCode": "+255"
//   },
//   {
//     "index": 203,
//     "zhTw": "泰國",
//     "en": "Thailand",
//     "phoneCode": "+66"
//   },
//   {
//     "index": 204,
//     "zhTw": "巴哈馬",
//     "en": "The Bahamas",
//     "phoneCode": "+1242"
//   },
//   {
//     "index": 205,
//     "zhTw": "岡比亞",
//     "en": "The Gambia",
//     "phoneCode": "+220"
//   },
//   {
//     "index": 206,
//     "zhTw": "多哥",
//     "en": "Togo",
//     "phoneCode": "+228"
//   },
//   {
//     "index": 207,
//     "zhTw": "托克勞",
//     "en": "Tokelau",
//     "phoneCode": "+690"
//   },
//   {
//     "index": 208,
//     "zhTw": "湯加",
//     "en": "Tonga",
//     "phoneCode": "+676"
//   },
//   {
//     "index": 209,
//     "zhTw": "特立尼達和多巴哥",
//     "en": "Trinidad and Tobago",
//     "phoneCode": "+1868"
//   },
//   {
//     "index": 210,
//     "zhTw": "突尼斯",
//     "en": "Tunisia",
//     "phoneCode": "+216"
//   },
//   {
//     "index": 211,
//     "zhTw": "土耳其",
//     "en": "Turkey",
//     "phoneCode": "+90"
//   },
//   {
//     "index": 212,
//     "zhTw": "土庫曼斯坦",
//     "en": "Turkmenistan",
//     "phoneCode": "+993"
//   },
//   {
//     "index": 213,
//     "zhTw": "特克斯和凱科斯群島",
//     "en": "Turks and Caicos Islands",
//     "phoneCode": "+1649"
//   },
//   {
//     "index": 214,
//     "zhTw": "圖瓦盧",
//     "en": "Tuvalu",
//     "phoneCode": "+688"
//   },
//   {
//     "index": 215,
//     "zhTw": "烏干達",
//     "en": "Uganda",
//     "phoneCode": "+256"
//   },
//   {
//     "index": 216,
//     "zhTw": "烏克蘭",
//     "en": "Ukraine",
//     "phoneCode": "+380"
//   },
//   {
//     "index": 217,
//     "zhTw": "阿拉伯聯合酋長國",
//     "en": "United Arab Emirates",
//     "phoneCode": "+971"
//   },
//   {
//     "index": 218,
//     "zhTw": "英國",
//     "en": "United Kingdom",
//     "phoneCode": "+44"
//   },
//   {
//     "index": 219,
//     "zhTw": "美國",
//     "en": "United States",
//     "phoneCode": "+1"
//   },
//   {
//     "index": 220,
//     "zhTw": "烏拉圭",
//     "en": "Uruguay",
//     "phoneCode": "+598"
//   },
//   {
//     "index": 221,
//     "zhTw": "烏茲別克斯坦",
//     "en": "Uzbekistan",
//     "phoneCode": "+998"
//   },
//   {
//     "index": 222,
//     "zhTw": "瓦努阿圖",
//     "en": "Vanuatu",
//     "phoneCode": "+678"
//   },
//   {
//     "index": 223,
//     "zhTw": "委內瑞拉",
//     "en": "Venezuela",
//     "phoneCode": "+58"
//   },
//   {
//     "index": 224,
//     "zhTw": "越南",
//     "en": "Vietnam",
//     "phoneCode": "+84"
//   },
//   {
//     "index": 225,
//     "zhTw": "美屬維爾京群島",
//     "en": "Virgin Islands",
//     "phoneCode": "+1340"
//   },
//   {
//     "index": 226,
//     "zhTw": "瓦利斯和富圖納",
//     "en": "Wallis and Futuna",
//     "phoneCode": "+681"
//   },
//   {
//     "index": 227,
//     "zhTw": "也門",
//     "en": "Yemen",
//     "phoneCode": "+967"
//   },
//   {
//     "index": 228,
//     "zhTw": "贊比亞",
//     "en": "Zambia",
//     "phoneCode": "+260"
//   },
//   {
//     "index": 229,
//     "zhTw": "津巴布韋",
//     "en": "Zimbabwe",
//     "phoneCode": "+263"
//   }
// ]
